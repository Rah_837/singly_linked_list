use std::{
    ptr::NonNull,
    marker::PhantomData,
    cmp::Ordering,
    fmt::{
        self,
        Formatter,
        Debug
    },
    hash::{
        Hasher,
        Hash
    },
    iter::{
        FromIterator,
        FusedIterator
    }
};



struct Node<T> {
    element: T,
    next: Option<NonNull<Node<T>>>
}

impl<T> Node<T> {
    #[inline]
    fn new(element: T) -> Self {
        Self {
            element,
            next: None
        }
    }
}




pub struct SinglyLinkedList<T> {
    head: Option<NonNull<Node<T>>>,
    len: usize
}

impl<T> SinglyLinkedList<T> {
    #[cfg(feature = "nightly")]
    #[inline]
    pub const fn new() -> Self {
        Self {
            head: None,
            len: 0
        }
    }

    #[cfg(not(feature = "nightly"))]
    #[inline]
    pub fn new() -> Self {
        Self {
            head: None,
            len: 0
        }
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.len
    }

    #[inline]
    pub fn front(&self) -> Option<&T> {
        self.head.map(|head| unsafe {
            &(*head.as_ptr()).element
        })
    }

     #[inline]
    pub fn front_mut(&mut self) -> Option<&mut T> {
        self.head.map(|head| unsafe {
            &mut (*head.as_ptr()).element
        })
    }

    #[inline]
    pub fn iter(&self) -> Iter<T> {
        Iter {
            next: self.head,
            len: self.len,

            marker: PhantomData
        }
    }

    #[inline]
    pub fn iter_mut(&self) -> IterMut<T> {
        IterMut {
            next: self.head,
            len: self.len,

            marker: PhantomData
        }
    }

    #[inline]
    pub fn cursor(&mut self) -> Cursor<T> {
        Cursor {
            current: None,
            list: self
        }
    }

    #[inline]
    pub fn push_front(&mut self, element: T) {
        let mut node = Box::new(Node::new(element));
        node.next = self.head;
        self.head = unsafe {
            Some(NonNull::new_unchecked(Box::into_raw(node)))
        };
        self.len += 1;
    }

    #[inline]
    pub fn pop_front(&mut self) -> Option<T> {
        self.head.map(|head| unsafe {
            let tmp = Box::from_raw(head.as_ptr());
            self.head = tmp.next;
            self.len -= 1;
            tmp.element
        })
    }

    #[inline]
    pub fn clear(&mut self) {
        while let Some(head) = self.head {
            self.head = unsafe {
                Box::from_raw(head.as_ptr()).next
            }
        }

        self.len = 0;
    }
}

impl<T> Drop for SinglyLinkedList<T> {
    #[inline]
    fn drop(&mut self) {
        self.clear();
    }
}

impl<T> Default for SinglyLinkedList<T> {
    #[inline]
    fn default() -> Self {
        Self::new()
    }
}

impl<'a, T: 'a + Copy> Extend<&'a T> for SinglyLinkedList<T> {
    fn extend<I: IntoIterator<Item = &'a T>>(&mut self, iter: I) {
        self.extend(iter.into_iter().cloned());
    }
}

impl<T> Extend<T> for SinglyLinkedList<T> {
    fn extend<I: IntoIterator<Item = T>>(&mut self, iter: I) {
        for e in iter {
            self.push_front(e);
        }
    }
}

impl<T> FromIterator<T> for SinglyLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list = Self::new();
        list.extend(iter);
        list
    }
}

impl<T: PartialEq> PartialEq for SinglyLinkedList<T> {
    fn eq(&self, other: &Self) -> bool {
        self.len() == other.len() && self.iter().eq(other)
    }

    fn ne(&self, other: &Self) -> bool {
        self.len() != other.len() || self.iter().ne(other)
    }
}

impl<T: Eq> Eq for SinglyLinkedList<T> {}

impl<T: PartialOrd> PartialOrd for SinglyLinkedList<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.iter().partial_cmp(other)
    }
}

impl<T: Ord> Ord for SinglyLinkedList<T> {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.iter().cmp(other)
    }
}

impl<T: Clone> Clone for SinglyLinkedList<T> {
    fn clone(&self) -> Self {
        self.iter().cloned().collect()
    }
}

impl<T: fmt::Debug> fmt::Debug for SinglyLinkedList<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list().entries(self).finish()
    }
}

impl<T: Hash> Hash for SinglyLinkedList<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.len().hash(state);
        for elt in self {
            elt.hash(state);
        }
    }
}

impl<T> IntoIterator for SinglyLinkedList<T> {
    type Item = T;
    type IntoIter = IntoIter<T>;

    #[inline]
    fn into_iter(self) -> IntoIter<T> {
        IntoIter {
            list: self
        }
    }
}

impl<'a, T> IntoIterator for &'a SinglyLinkedList<T> {
    type Item = &'a T;
    type IntoIter = Iter<'a, T>;

    fn into_iter(self) -> Iter<'a, T> {
        self.iter()
    }
}

impl<'a, T> IntoIterator for &'a mut SinglyLinkedList<T> {
    type Item = &'a mut T;
    type IntoIter = IterMut<'a, T>;

    fn into_iter(self) -> IterMut<'a, T> {
        self.iter_mut()
    }
}

unsafe impl<T: Send> Send for SinglyLinkedList<T> {}

unsafe impl<T: Sync> Sync for SinglyLinkedList<T> {}




#[derive(Clone)]
pub struct IntoIter<T> {
    list: SinglyLinkedList<T>,
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    #[inline]
    fn next(&mut self) -> Option<T> {
        self.list.pop_front()
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.list.len, Some(self.list.len))
    }
}

impl<T: fmt::Debug> fmt::Debug for IntoIter<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_tuple("IntoIter")
         .field(&self.list)
         .finish()
    }
}

impl<T> ExactSizeIterator for IntoIter<T> {}

impl<T> FusedIterator for IntoIter<T> {}




#[derive(Clone)]
pub struct Iter<'a, T: 'a> {
    next: Option<NonNull<Node<T>>>,
    len: usize,
    
    marker: PhantomData<&'a Node<T>>,
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    #[inline]
    fn next(&mut self) -> Option<&'a T> {
        self.next.map(|next| unsafe {
            self.len -= 1;
            self.next = next.as_ref().next;
            &(*next.as_ptr()).element
        })
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T: 'a + Debug> Debug for Iter<'a, T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_tuple("Iter")
         .field(&self.len)
         .finish()
    }
}

impl<'a, T> ExactSizeIterator for Iter<'a, T> {}

impl<'a, T> FusedIterator for Iter<'a, T> {}

unsafe impl<'a, T: Sync> Send for Iter<'a, T> {}

unsafe impl<'a, T: Sync> Sync for Iter<'a, T> {}




pub struct IterMut<'a, T: 'a> {
    next: Option<NonNull<Node<T>>>,
    len: usize,
    
    marker: PhantomData<&'a Node<T>>,
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    #[inline]
    fn next(&mut self) -> Option<&'a mut T> {
        self.next.map(|next| unsafe {
            self.len -= 1;
            self.next = next.as_ref().next;
            &mut (*next.as_ptr()).element
        })
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}

impl<'a, T: 'a + Debug> Debug for IterMut<'a, T> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        f.debug_tuple("IterMut")
         .field(&self.len)
         .finish()
    }
}

impl<'a, T> ExactSizeIterator for IterMut<'a, T> {}

impl<'a, T> FusedIterator for IterMut<'a, T> {}

unsafe impl<'a, T: Send> Send for IterMut<'a, T> {}

unsafe impl<'a, T: Sync> Sync for IterMut<'a, T> {}




pub struct Cursor<'a, T: 'a> {
    current: Option<NonNull<Node<T>>>,
    list: &'a mut SinglyLinkedList<T>,
}

impl<'a, T> Cursor<'a, T> {
    #[inline]
    pub fn reset(&mut self) {
        self.current = self.list.head;
    }

    #[inline]
    pub fn next(&mut self) -> Option<&mut T> {
        match self.current {
            None        => unsafe {
                self.reset();
                self.list.head.map(|head| &mut (*head.as_ptr()).element)
            },
            Some(curr)  => unsafe {
                self.current = curr.as_ref().next;
                self.current.map(|curr| &mut (*curr.as_ptr()).element)
            }
        }
    }

    #[inline]
    pub fn peek(&mut self) -> Option<&mut T> {
        self.current.map(|curr| unsafe {
            &mut (*curr.as_ptr()).element
        })
    }

    #[inline]
    pub fn peek_next(&mut self) -> Option<&mut T> {
        self.current.map_or(None, |curr| unsafe {
            (*curr.as_ptr()).next.map(|next| &mut (*next.as_ptr()).element)
        })
    }

    #[inline]
    pub fn insert_next(&mut self, element: T) {
        let mut node = Box::new(Node::new(element));

        match self.current {
            None                => {
                node.next = self.list.head;
                self.list.head = unsafe {
                    Some(NonNull::new_unchecked(Box::into_raw(node)))
                };
            },
            Some(ref mut curr)  => unsafe {
                node.next = curr.as_ref().next;
                curr.as_mut().next = Some(NonNull::new_unchecked(Box::into_raw(node)));
            }
        }

        self.list.len += 1;
    }

    #[inline]
    pub fn remove_next(&mut self) {
        if let Some(ref mut curr) = self.current {
            unsafe {
                if let Some(next) = (*curr.as_ptr()).next {
                    let tmp = Box::from_raw(next.as_ptr());
                    curr.as_mut().next = tmp.next;
                    self.list.len -= 1;           
                }
            }
        }
    }
}




#[cfg(test)]
mod tests {
    use super::SinglyLinkedList;



    #[test]
    fn basis() {
        let mut list = SinglyLinkedList::new();

        assert!(list.is_empty());
        assert_eq!(list.len(), 0);

        list.extend(&[3u8, 2, 1, 1]);

        assert!(!list.is_empty());
        assert_eq!(list.len(), 4);

        assert_eq!(list.front(), Some(&1));
        *list.front_mut().unwrap() = 0;
        assert_eq!(list.front(), Some(&0));

        assert_eq!(list.pop_front(), Some(0));
        assert_eq!(list.len(), 3);

        list.clear();
        assert!(list.is_empty());
        assert_eq!(list.len(), 0);
    }

    #[test]
    fn iter() {
        let mut list = SinglyLinkedList::new();
        list.extend(&[3u8, 2, 1, 0]);

        for (i, e) in list.iter().enumerate() {
            assert_eq!(*e as usize, i);
        }

        let mut iter1 = list.iter();
        assert_eq!(iter1.next(), Some(&0));

        let mut iter2 = iter1.clone();
        assert_eq!(iter2.next(), Some(&1));
    }

    #[test]
    fn iter_mut() {
        let mut list = SinglyLinkedList::<u8>::new();
        list.extend(&[3u8, 2, 1, 0]);

        for e in list.iter_mut() {
            *e += 1;
        }

        for (i, e) in list.iter().enumerate() {
            assert_eq!(*e as usize, i + 1);
        }
    }

    #[test]
    fn cursor() {
        let mut list = SinglyLinkedList::new();

        {
            let mut cursor = list.cursor();

            assert_eq!(cursor.peek(), None);
            assert_eq!(cursor.peek_next(), None);

            cursor.insert_next(0u8);
        }

        assert_eq!(list.front(), Some(&0));

        let mut cursor = list.cursor();

        assert_eq!(cursor.next(), Some(&mut 0u8));
        assert_eq!(cursor.peek(), Some(&mut 0u8));

        cursor.insert_next(1);
        assert_eq!(cursor.peek_next(), Some(&mut 1u8));

        cursor.insert_next(2);
        assert_eq!(cursor.next(), Some(&mut 2u8));

        cursor.insert_next(3);
        assert_eq!(cursor.peek_next(), Some(&mut 3u8));
        cursor.remove_next();
        assert_eq!(cursor.peek_next(), Some(&mut 1));
    }
}